begin
  require 'dke/ruby_provider'
rescue LoadError
  warn('Your dke RubyGem is missing or out of date.',
       'Install the latest version using `gem install dke`.')
end
