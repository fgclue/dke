#!/usr/bin/env bash

set -e
set -o pipefail

# Use default CC to avoid compilation problems when installing Python modules.
echo "Install dke module for Python."
CC=cc python -m pip -q install --user --upgrade pynvim

echo "Install dke RubyGem."
gem install --no-document --bindir "$HOME/.local/bin" --user-install --pre dke

echo "Install dke npm package"
npm install -g dke
npm link dke

sudo cpanm -n dke::Ext || cat "$HOME/.cpanm/build.log"
perl -W -e 'use dke::Ext; print $dke::Ext::VERSION'
